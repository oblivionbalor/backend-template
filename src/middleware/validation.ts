import { RequestHandler } from "express";
import { body, param } from "express-validator";
import { TCourseValidator } from "../interfaces/validators";

const mongoIDValidator: RequestHandler[] = [
  param("id")
    .exists()
    .withMessage("Product ID must be provided")
    .bail()
    //   .matches(/^[a-f\d]{24}$/i)
    .isMongoId()
    .withMessage("ID is not in valid mongoDB format"),
];

const courseValidator: TCourseValidator = {
  create: [
    body("title")
      .exists()
      .withMessage("title was not provided")
      .bail()
      .notEmpty()
      .withMessage("title cannot be empty")
      .bail()
      .isString()
      .withMessage("title must be a string"),
    body("author")
      .exists()
      .withMessage("author was not provided")
      .bail()
      .notEmpty()
      .withMessage("author cannot be empty")
      .bail()
      .isString()
      .withMessage("author must be a string"),
    body("category")
      // .optional()
      .exists()
      .withMessage("category was not provided")
      .bail()
      .notEmpty()
      .withMessage("category cannot be empty")
      .bail()
      .isString()
      .withMessage("category must be a string"),
    body("chapters")
      .exists()
      .withMessage("Chapters must be provided")
      .bail()
      .notEmpty()
      .withMessage("Chapters cannot be empty")
      .bail()
      .isArray()
      .withMessage("Chapters must be an array"),
    body("description")
      .exists()
      .withMessage("description was not provided")
      .bail()
      .notEmpty()
      .withMessage("description cannot be empty")
      .bail()
      .isString()
      .withMessage("description must be a string"),
    body("releaseDate")
      .exists()
      .withMessage("releaseDate was not provided")
      .bail()
      .notEmpty()
      .withMessage("releaseDate cannot be empty")
      .bail()
      .isDate({ format: "YYYY-MM-DD" })
      .withMessage("Invalid release date format (YYYY-MM-DD)"),
    body("price")
      .exists()
      .withMessage("price was not provided")
      .bail()
      .notEmpty()
      .withMessage("price cannot be empty")
      .bail()
      .isFloat({ min: 1, max: 1000 })
      .withMessage("Price must be a positive number"),
  ],
  update: [
    body("name")
      .optional()
      .notEmpty()
      .withMessage("Name is required")
      .bail()
      .isString()
      .withMessage("name must be a string"),
    body("author")
      .optional()
      .notEmpty()
      .withMessage("author is required")
      .bail()
      .isString()
      .withMessage("author must be a string"),
    body("category")
      .optional()
      .notEmpty()
      .withMessage("category cannot be empty")
      .bail()
      .isString()
      .withMessage("category must be a string"),
    body("description")
      .optional()
      .notEmpty()
      .withMessage("description cannot be empty")
      .bail()
      .isString()
      .withMessage("description must be a string"),
    body("releaseDate")
      .optional()
      .notEmpty()
      .withMessage("releaseDate cannot be empty")
      .bail()
      .isDate({ format: "YYYY-MM-DD" })
      .withMessage("Invalid release date format (YYYY-MM-DD)"),
    body("pages")
      .optional()
      .notEmpty()
      .withMessage("pages cannot be empty")
      .bail()
      .isInt({ min: 1 })
      .withMessage("Pages must be a positive integer and bigger than 0"),
    body("price")
      .optional()
      .notEmpty()
      .withMessage("price cannot be empty")
      .bail()
      .isFloat({ min: 1, max: 1000 })
      .withMessage("Price must be a positive number"),
    body("stock")
      .optional()
      .notEmpty()
      .withMessage("stock cannot be empty")
      .bail()
      .isInt({ min: 1 })
      .withMessage("Stock must be a non-negative integer"),
  ],
};

const AuthValidator = {
  login: [
    body("email")
      .exists()
      .withMessage("Email must be provided")
      .bail()
      .notEmpty()
      .withMessage("Email cannot be empty"),
    body("password")
      .exists()
      .withMessage("Password must be provided")
      .bail()
      .notEmpty()
      .withMessage("Password cannot be empty"),
  ],
  signup: [
    body("name")
      .exists()
      .withMessage("Name must be provided")
      .bail()
      .isString()
      .withMessage("Name must be a string")
      .bail()
      .matches(/^[a-zA-Z-. ]*$/)
      .withMessage("Name must be in only alphabets")
      .isLength({ min: 1, max: 100 })
      .withMessage("Name must be between 1 and 100 characters")
      .bail(),
    body("email")
      .exists()
      .withMessage("Email must be provided")
      .bail()
      .isString()
      .withMessage("Email must be a string")
      .bail()
      .isEmail()
      .withMessage("Email must be in valid format"),
    body("password")
      .exists()
      .withMessage("Password must be provided")
      .bail()
      .isString()
      .withMessage("Password must be a string")
      .bail()
      .isStrongPassword({
        minLength: 8,
        minLowercase: 1,
        minUppercase: 1,
        minSymbols: 1,
        minNumbers: 1,
      })
      .withMessage(
        "Password must contain at least 8 characters with 1 lower case, 1 upper case, 1 number, 1 symbol"
      ),
    body("passwordConfirm")
      .exists()
      .withMessage("Password must be provided")
      .bail()
      .isString()
      .withMessage("Password must be a string")
      .bail()
      .custom((value, { req }) => {
        if (value === req.body.password) {
          return true;
        }
        throw new Error("Passwords do not match");
      }),
    body("phone")
      .exists()
      .withMessage("Phone number must be provided")
      .bail()
      .isString()
      .withMessage("Phone number must be a string")
      .bail()
      .matches(/(^(\+88|0088)?(01){1}[3456789]{1}(\d){8})$/)
      .withMessage("Phone number must be in a valid format"),
    body("address.city")
      .exists()
      .withMessage("City was not provided")
      .bail()
      .isString()
      .withMessage("City must be a string"),
    body("address.country")
      .exists()
      .withMessage("Country was not provided")
      .bail()
      .isString()
      .withMessage("Country must be a string"),
  ],
};

export { mongoIDValidator, courseValidator, AuthValidator };
