import mongoose from "mongoose";

const chapterSchema = new mongoose.Schema(
  {
    title: {
      type: String,
      required: [true, "title was not provided"],
      minLength: 1,
    },
    subChapters: {
      type: [
        {
          title: String,
          video: String,
          comment: String,
          quiz: {
            type: mongoose.Types.ObjectId,
            ref: "Quiz",
          },
          assignment: {
            type: mongoose.Types.ObjectId,
            ref: "Assignment",
          },
        },
      ],
    },
    description: {
      type: String,
      required: [true, "description was not provided"],
    },
    quizzes: [
      {
        type: mongoose.Types.ObjectId,
        ref: "Quiz",
      },
    ],
    assignments: [
      {
        type: mongoose.Types.ObjectId,
        ref: "Assignment",
      },
    ],
    course: {
      type: mongoose.Types.ObjectId,
      ref: "Course",
    },
  },
  { timestamps: true }
);

const ChapterModel = mongoose.model("Chapter", chapterSchema);

export default ChapterModel;
