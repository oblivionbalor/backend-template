import mongoose, { Document, Schema, Model, Types } from "mongoose";

export interface INotification {
  userId: Types.ObjectId;
  message: string;
  isRead: boolean;
  createdAt: Date;
}

export type INotificationModel = Model<INotification>;

const notificationSchema = new Schema<INotification>({
  userId: { type: mongoose.Schema.Types.ObjectId, required: true },
  message: { type: String, required: true },
  isRead: { type: Boolean, default: false },
  createdAt: { type: Date, default: Date.now },
});

const NotificationModel: INotificationModel = mongoose.model<INotification>(
  "Notification",
  notificationSchema
);

export default NotificationModel;
