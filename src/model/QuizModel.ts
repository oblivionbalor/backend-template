import mongoose, { Document, Schema, Model, Types } from "mongoose";

export interface IQuestion {
  text: string;
  options: string[];
  correctOptionIndex: number;
}

export interface IQuiz extends Document {
  title: string;
  questions: IQuestion[];
  createdBy: Types.ObjectId;
  chapters: Types.ObjectId[];
  courses: Types.ObjectId[];
  isDeleted: boolean;
}

export type IQuizModel = Model<IQuiz>;

const quizSchema = new Schema<IQuiz>({
  title: { type: String, required: true },
  questions: [
    {
      text: { type: String, required: true },
      options: { type: [String], required: true },
      correctOptionIndex: { type: Number, required: true },
    },
  ],
  createdBy: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Instructor",
    required: true,
  },
  chapters: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Chapter",
      // required: true,
    },
  ],
  courses: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Course",
      // required: true,
    },
  ],
  isDeleted: { type: Boolean, default: false }
});

const QuizModel: IQuizModel = mongoose.model<IQuiz>("Quiz", quizSchema);

export default QuizModel;
