import mongoose, { Types } from "mongoose";

const courseSchema = new mongoose.Schema(
  {
    title: {
      type: String,
      required: [true, "title was not provided"],
      minLength: 1,
    },
    category: {
      type: String,
      required: [true, "category was not provided"],
    },
    chapters: [
      {
        // type: [{name: String, _id:false}],
        type: mongoose.Types.ObjectId,
        ref: "Chapter",
        required: false,
      },
    ],
    students: [
      {
        // type: [{name: String, _id:false}],
        type: mongoose.Types.ObjectId,
        ref: "Student",
      },
    ],
    instructors: [
      {
        // type: [{name: String, _id:false}],
        type: mongoose.Types.ObjectId,
        ref: "Instructor",
      },
    ],
    // quizzes: [
    //   {
    //     // type: [{name: String, _id:false}],
    //     type: mongoose.Types.ObjectId,
    //     ref: "Quiz",
    //   },
    // ],
    description: {
      type: String,
      required: [true, "description was not provided"],
    },
    releaseDate: {
      type: Date,
      // required: [true, 'date wasnt provided'],
    },
    img: {
      type: String,
    },
    reviews: {
      type: Types.ObjectId,
      ref: "Review",
      default: [],
    },
    // discount: {
    //   type: mongoose.Types.ObjectId,
    //   ref: "Discount",
    //   // required: true,
    // },
  },
  { timestamps: true }
);

const CourseModel = mongoose.model("Course", courseSchema);

export default CourseModel;
