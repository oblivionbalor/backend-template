// const express = require("express");
import express from "express";
const routes = express();
// const ProductController = require("../controller/CourseController.ts");

import CourseController from "../controller/CourseController.ts";

import { mongoIDValidator, courseValidator } from "../middleware/validation.ts";
// const { productValidator } = require("../middleware/validation");
// const upload = require("../config/files");
// const createValidation = require("../middleware/validation");
// const createValidationPartial = require("../middleware/validationPartial");

// const { isAuthorized } = require("../middleware/authValidationJWT");

// routes.get("/getall", ProductController.getAllProducts);

// requirement
// gets all data
routes.get("/getall", CourseController.getAll);

// get one data
routes.get("/:id", mongoIDValidator, CourseController.getOne);

// deletes
routes.delete(
  "/:id",
  // isAuthorized,
  mongoIDValidator,
  CourseController.delete
);

// add
routes.post(
  "/add",
  //   upload.single("file_to_upload"),
  //   isAuthorized,
  //   productValidator.create,
  // courseValidator.create,
  CourseController.add
);

// partial update
routes.patch(
  "/update/:id",
  // isAuthorized,
  // productValidator.update,
  CourseController.update
);

// module.exports = routes;
export default routes;
