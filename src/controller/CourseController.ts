import express, { Request, Response } from "express";
import mongoose from "mongoose";
// const HTTP_STATUS = require("../constants/statusCodes");
import HTTP_STATUS from "../constants/statusCodes.ts";
// const { validationResult } = require("express-validator");
import { validationResult, ValidationError } from "express-validator";
// const { success, failure } = require("../utilities/common");
import sendResponse from "../util/common.ts";
// const ProductModel = require("../model/Product");
import CourseModel from "../model/CourseModel.ts";
import CourseRepository from "../repository/Course/index.ts";
// const DiscountModel = require("../model/Discount");

class Course {
  async getAll(req: Request, res: Response) {
    try {
      // const {
      //   sortParam,
      //   sortOrder,
      //   search,
      //   name,
      //   author,
      //   price,
      //   priceFil,
      //   stock,
      //   stockFil,
      //   page,
      //   limit,
      // } = req.query;
      // if (page < 1 || limit < 0) {
      //   return res
      //     .status(HTTP_STATUS.UNPROCESSABLE_ENTITY)
      //     .send(failure("Page and limit values must be at least 1"));
      // }
      // if (
      //   (sortOrder && !sortParam) ||
      //   (!sortOrder && sortParam) ||
      //   (sortParam &&
      //     sortParam !== "stock" &&
      //     sortParam !== "price" &&
      //     sortParam !== "name") ||
      //   (sortOrder && sortOrder !== "asc" && sortOrder !== "desc")
      // ) {
      //   return res
      //     .status(HTTP_STATUS.UNPROCESSABLE_ENTITY)
      //     .send(failure("Invalid sort parameters provided"));
      // }
      // const filter = {};

      // if (price && priceFil) {
      //   if (priceFil === "low") {
      //     filter.price = { $lte: parseFloat(price) };
      //   } else {
      //     filter.price = { $gte: parseFloat(price) };
      //   }
      // }
      // if (stock && stockFil) {
      //   if (stockFil === "low") {
      //     filter.stock = { $lte: parseFloat(stock) };
      //   } else {
      //     filter.stock = { $gte: parseFloat(stock) };
      //   }
      // }

      // if (name) {
      //   filter.name = { $regex: name, $options: "i" };
      // }
      // if (author) {
      //   filter.author = { $in: author.toLowerCase() };
      // }
      // if (search) {
      //   filter["$or"] = [
      //     { name: { $regex: search, $options: "i" } },
      //     { author: { $regex: search, $options: "i" } },
      //   ];
      // }
      // console.log(filter.$or);
      // console.log(typeof Object.keys(JSON.parse(JSON.stringify(filter)))[0]);
      // const productCount = await ProductModel.find({}).count();
      // const products = await ProductModel.find(filter)
      //   .sort({
      //     [sortParam]: sortOrder === "asc" ? 1 : -1,
      //   })
      //   .skip((page - 1) * limit)
      //   .limit(limit ? limit : 10);
      // // console.log(products)
      // if (products.length === 0) {
      //   return res.status(HTTP_STATUS.OK).send(
      //     success("No products were found", {
      //       total: productCount,
      //       totalPages: null,
      //       count: 0,
      //       page: 0,
      //       limit: 0,
      //       products: [],
      //     })
      //   );
      // }

      const { courses, courseCount } = await CourseRepository.getAll();

      // console.log(courses);
      if (courses) {
        console.log("ip", req.socket.remoteAddress);
        return sendResponse(
          res,
          HTTP_STATUS.OK,
          "Successfully got all the courses",
          {
            total: courseCount,
            // count: products.length,
            // page: parseInt(page),
            // limit: parseInt(limit),
            courses: courses,
          }
        );
      } else {
        return sendResponse(res, HTTP_STATUS.NOT_FOUND, "No Courses found");
      }
    } catch (error) {
      console.log(error);
      return sendResponse(
        res,
        HTTP_STATUS.INTERNAL_SERVER_ERROR,
        "Internal server error"
      );
    }
  }

  // gets only one product
  async getOne(req: Request, res: Response) {
    try {
      const validation: ValidationError[] = validationResult(req).array();
      // console.log(validation);
      if (validation.length > 0) {
        return sendResponse(
          res,
          HTTP_STATUS.NOT_FOUND,
          "Failed to get the course",
          validation[0].msg
        );
      }

      const { id } = req.params;

      // const course = await CourseModel.find({ _id: id });

      // const mongooseID = mongoose.Types.ObjectId(id)
      const course = await CourseRepository.getOne(
        new mongoose.Types.ObjectId(id)
      );
      if (course.length) {
        // return res.status(HTTP_STATUS.OK).send(course[0]);
        return sendResponse(
          res,
          HTTP_STATUS.OK,
          "Successfully got the course",
          course
        );
      } else {
        return sendResponse(
          res,
          HTTP_STATUS.NOT_FOUND,
          "Course with this ID doesnt exist"
        );
      }
    } catch (error) {
      console.log("error", error);
      return sendResponse(
        res,
        HTTP_STATUS.BAD_REQUEST,
        "internal server error"
      );
    }
  }

  // // adds
  async add(req: Request, res: Response) {
    try {
      const validation = validationResult(req).array();
      // console.log(validation);
      if (validation.length > 0) {
        return sendResponse(
          res,
          HTTP_STATUS.NOT_FOUND,
          "Failed to add the course",
          validation[0].msg
        );
      }
      const {
        title,
        price,
        author,
        category,
        chapters,
        description,
        releaseDate,
      } = req.body;

      console.log("Req Body", req.body);
      // const img = req.file.filename;
      // console.log("img", img);

      const course = await CourseRepository.add(
        title,
        price,
        author,
        category,
        chapters,
        description,
        releaseDate
      );

      return sendResponse(
        res,
        HTTP_STATUS.CREATED,
        "Course Added Successfully",
        course
      );
    } catch (error) {
      console.log("error", error);
      return sendResponse(
        res,
        HTTP_STATUS.INTERNAL_SERVER_ERROR,
        "internal server error"
      );
    }
  }

  // deletes a product
  async delete(req: Request, res: Response) {
    try {
      const validation = validationResult(req).array();
      // console.log(validation);
      if (validation.length > 0) {
        return sendResponse(
          res,
          HTTP_STATUS.NOT_FOUND,
          "Failed to remove the course",
          validation[0].msg
        );
      }
      const itemId = req.params.id;
      // Find the item by ID and delete it
      const deletedItem = await CourseModel.findByIdAndDelete(itemId);
      console.log("deleted course", deletedItem);

      if (!deletedItem) {
        return sendResponse(res, HTTP_STATUS.NOT_FOUND, "course not found");
      }

      return sendResponse(
        res,
        HTTP_STATUS.ACCEPTED,
        "course deleted successfully",
        deletedItem
      );
    } catch (error) {
      console.error(error);
      return sendResponse(
        res,
        HTTP_STATUS.INTERNAL_SERVER_ERROR,
        "Internal server error"
      );
    }
  }

  // updates
  async update(req: Request, res: Response) {
    try {
      const courseId = req.params.id;
      const updatedCourseData = req.body;

      // const validation = validationResult(req).array();

      // if (validation.length > 0) {
      //     return res
      //         .status(HTTP_STATUS.OK)
      //         .send(failure("Failed to update data", validation[0].msg));
      // }

      const updatedCourse = await CourseRepository.update(
        new mongoose.Types.ObjectId(courseId),
        updatedCourseData
      );

      if (!updatedCourse) {
        return sendResponse(
          res,
          HTTP_STATUS.NOT_FOUND,
          "Failed to update. course not found"
        );
      }
      console.log("updatedCourse", updatedCourse);

      return sendResponse(
        res,
        HTTP_STATUS.ACCEPTED,
        "course updated successfully",
        updatedCourse
      );
    } catch (error) {
      return res
        .status(HTTP_STATUS.INTERNAL_SERVER_ERROR)
        .json({ message: "INTERNAL SERVER ERROR" });
    }
  }
}

export default new Course();
