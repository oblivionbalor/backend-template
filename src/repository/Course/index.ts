import mongoose from "mongoose";
import CourseModel from "../../model/CourseModel.ts";

class CourseRepository {
  async getAll() {
    const courses = await CourseModel.find({}).populate("chapters");
    const courseCount = await CourseModel.find({}).count();
    return { courses, courseCount };
  }

  async getOne(id: mongoose.Types.ObjectId) {
    const course = await CourseModel.find({ _id: id }).populate("chapters");
    return course;
  }

  async add(
    title: string,
    price: number,
    author: string,
    category: string,
    chapters: [Object],
    description: string,
    releaseDate: Date
  ) {
    const course = new CourseModel({
      title,
      price,
      author,
      category,
      chapters,
      description,
      releaseDate,
      // img,
    });

    await course.save();

    return course;
  }

  async update(courseId: mongoose.Types.ObjectId, updatedCourseData: Object) {
    const updatedCourse = await CourseModel.findByIdAndUpdate(
      courseId,
      updatedCourseData,
      // Returns the updated document
      { new: true }
    );

    return updatedCourse;
  }
}

export default new CourseRepository();
