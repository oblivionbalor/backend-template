import { Response }  from "express";

type TResponse = {
    success: boolean,
    message: string,
    error?: null | string,
    data?: any
}

const sendResponse : (res: Response, status: number, message: string, result?: any) => Response<any, Record<string, any>> = (res, status, message, result = null) => {
    const response: TResponse = {success:false, message: ""};
    if (status >= 400) {
        response.success = false;
        response.error = result;
        response.message = "Internal server error";
    } else {
        response.success = true;
        response.data = result;
        response.message = "Successfully completed operations";
    }

    if (message) {
        response.message = message;
    }
    return res.status(status).send(response);
};

// module.exports = { sendResponse };
export default sendResponse